import typing
import uuid
import strawberry
from generate_ques import generate_multiple_choice
from fastapi_sqlalchemy import db
from presets import PresetsCollection as Presets
from gql_types import RequestGeneratedQuestionBlock, GeneratedQuestionBlock, BasicOutput
from models import GeneratedQuestions, GeneratedAnswers


@strawberry.type
class Mutation:
    @strawberry.mutation
    def generate_question_block(
        self, info, ques_block: RequestGeneratedQuestionBlock
    ) -> BasicOutput:
        args = {
            "context_size": 4,
            "max_questions": ques_block.ques_numb,
            "summarize_ratio": None,
            "temperature_answer": 0.5,
            "temperature_question": 0.5,
            "temperature_wrong_answer": 2.0,
            "summarize_word_count": 3000,
            "generate_count": 5,
            "generate_size": 1,
            "answers": ques_block.answ_numb,
            "wrong_context_size": 0,
            "text": ques_block.text,
        }
        question_block = generate_multiple_choice(args)
        if question_block:
            for q_a in question_block:
                question = GeneratedQuestions(
                    title=q_a["question"], lesson_uuid=ques_block.lesson_uuid
                )
                db.session.add(question)
                db.session.flush()
                uuid_ = question.uuid
                correct_answer = GeneratedAnswers(
                    title=q_a["answer"], correct=True, question_id=uuid_
                )
                db.session.add(correct_answer)
                for answer in q_a["wrong"]:
                    wrong_answer = GeneratedAnswers(title=answer, question_id=uuid_)
                    db.session.add(wrong_answer)
            db.session.commit()
            db.session.close()
            return BasicOutput(**Presets().action_success_generation)
        return BasicOutput(**Presets().action_not_enough_text)


@strawberry.type
class Query:
    @strawberry.field
    def get_generated_questions(
        self, lesson_uuid: uuid.UUID
    ) -> typing.List[GeneratedQuestionBlock]:
        questions = db.session.query(GeneratedQuestions).filter_by(
            lesson_uuid=lesson_uuid
        )
        return questions


schema = strawberry.Schema(mutation=Mutation, query=Query)
