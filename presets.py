from dataclasses import dataclass, field
from typing import Dict


@dataclass
class PresetsCollection:

    action_success_generation: Dict[str, bool] = field(
        default_factory=lambda: {
            "success": True,
            "message": "Questions successfully generated",
        }
    )

    action_not_enough_text: Dict[str, bool] = field(
        default_factory=lambda: {
            "success": False,
            "message": "Not enough text",
        }
    )
