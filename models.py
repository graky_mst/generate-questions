from uuid import uuid4

import sqlalchemy
from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class GeneratedQuestions(Base):
    __tablename__ = "generated_questions"
    uuid = Column(UUID(as_uuid=True), default=uuid4, primary_key=True)
    lesson_uuid = Column(UUID(as_uuid=True))
    title = Column(sqlalchemy.Text, nullable=True)
    answers = relationship("GeneratedAnswers", back_populates="question")


class GeneratedAnswers(Base):
    __tablename__ = "generated_answers"
    uuid = Column(UUID(as_uuid=True), default=uuid4, primary_key=True)
    title = Column(sqlalchemy.Text, nullable=True)
    correct = Column(sqlalchemy.Boolean, nullable=False, default=False)
    question_id = Column(
        UUID(as_uuid=True), sqlalchemy.ForeignKey("generated_questions.uuid")
    )
    question = relationship("GeneratedQuestions", back_populates="answers")

    def __repr__(self):
        return self.title
