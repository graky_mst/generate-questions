import typing
import strawberry
import uuid


@strawberry.input
class RequestGeneratedQuestionBlock:
    text: str
    ques_numb: int
    answ_numb: int
    lesson_uuid: uuid.UUID


@strawberry.type
class GeneratedQuestionBlock:
    title: str
    answers: typing.List[str]
    lesson_uuid: uuid.UUID


@strawberry.type
class BasicOutput:
    success: bool
    message: str
